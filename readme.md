#Assignment 3 | Adam Smith | LIS4381
DOUBLE CHECK CARDINALITIES IN ERD
![ERD Screenshot](a3.png "ERD Screenshot for A3")
##A3 Notes:
>a3.sql needed by webhost
>"local" connection in MySQLWorkbench uses default PW for Ampps? ("mysql"?)
>Arvixe *may* not allow secure connection, hence uploading the SQL file
>
##Assignment Questions
1. c (logic error)
2. d (syntax error)
3. a (runtime error)
4. c (echo statements)
5. b (find all errors in the application)
6. a (fix all the errors in the application)
7. c (response is returned to server to request another page)
8. b (have the PHP file run itself again)
9. b (the database & the view)
10. d (view)
11. d (data/ database)
12. a (make each layer as independent as possible)
13. c?
14. a (function)
15. d (paramaters)
16. b (arguments)
17. c (return statement)
18. Correct order: (3: check user interface), (2: test w/ valid data), (4: test w/ invalid data), (1: try to make application fail)
19. a ($product = get_product($product_id);)
20. d(?)

##SQL Statements
1. "List only the pet store IDs, full address, and phone number for all of the pet stores"
>SELECT str_id, str_street, str_city, str_zip, str_state, str_phone
>FROM store;

2. "Display the pet store name, along with the number of pets each pet store has."
>SELECT str_name, COUNT(pet_id) as 'number of pets'
>FROM store
>NATURAL JOIN pet
>GROUP BY str_id;

3. "List each pet store ID, along with all of the customer first, last names and balances associated with each pet store."
>SELECT str_id, cus_fname, cus_lname, cus_balance
>FROM store
> NATURAL JOIN pet
> NATURAL JOIN customer;

4. "Update the customer last name to 'Tidwall' for Customer #4."
>SELECT * FROM customer;
> 
>UPDATE customer
>SET cus_lname = "Tidwall"
>WHERE cus_id = "4";
> 
>SELECT * FROM customer;

5. "Remove pet #2."
>SELECT * FROM pet;
> 
>DELETE FROM pet
>WHERE pet_id = "2";
> 
>SELECT * FROM pet;

6. "Add two more customers."
>SELECT * FROM customer;
> 
>INSERT INTO customer
>(cus_id,cus_fname,cus_lname,cus_street,cus_city,cus_state,cus_zip,cus_phone,cus_email,cus_url,cus_balance,cus_total_sales,cus_note)
>VALUES
>(null,jimmy,chang,rough,tallahassee,GA,321544567,


